# Quazi-Modded

[![Discord Server](https://img.shields.io/discord/594411513457147904.svg?color=blueviolet)](https://discord.io/origami)

Quazi-Modded is a Minecraft mod that aims to add smaller, standalone features to survival mode.  It has a focus on consistent, vanilla-style art direction and creating an atmosphere.

https://origami-games.github.io/quazi-modded

## Building
1. JDK 8 is required. Install it using https://adoptopenjdk.net
2. Download the [sources](https://gitlab.com/origami-games/quazi-modded.forge/-/archive/master/quazi-modded.forge-master.zip) to a folder
2. Open a terminal window in the same directory as the sources (`shift` + `right click` while inside the desired folder and `Open PowerShell window here`) and run `./gradlew build`
3. After some time, the built mod will be in `/build/libs`.

## Installation (Users)
Quazi-Modded (Forge) uses *Forge* as it's mod API. Refer to their installation instructions [here](https://files.minecraftforge.net).

Once you have Forge installed, simply download the latest version of Quazi-Modded (Forge) from [CurseForge](https://curseforge.com/minecraft/mc-mods/quazi-modded-forge/files) and place it in your `mods` folder.
**Remember to use the Forge launcher profile when starting the game!**
